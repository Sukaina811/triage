package ca.kenma.triage.container;

/**
 * A container with methods to save and load all TriageData, and Authentication
 * methods (login and register) with Accounts in the Triage application.
 * 
 *
 * 
 */
public class TriageApplication extends Application<TriageData> implements
		Accounts<Account>, Authentication<Account> {

	private Account activeAccount = null;

	/**
	 * Constructs a new instance of TriageApplication.
	 */
	public TriageApplication() {

		// start up the Data instance
		super(TriageData.class);
	}

	@Override
	public Account getAccount(final String username) {
		return this.getAppData().getAccount(username);
	}

	@Override
	public Account getAccount(final String username, final String password) {
		return this.getAppData().getAccount(username, password);
	}

	@Override
	public boolean addAccount(final Account account) {
		return this.getAppData().addAccount(account);
	}

	@Override
	public Account getActiveAccount() {
		return this.activeAccount;
	}

	@Override
	public void setActiveAccount(final Account account) {
		this.activeAccount = account;
	}

	@Override
	public boolean isLoggedIn() {
		return (this.getActiveAccount() != null);
	}

	/**
	 * Initializes a brand new instance of this TriageApplication and records a
	 * path to the appdata file that retains all saved TriageData on the 
	 * system.
	 * 
	 * @param pathToDataFile
	 *            the path of the file to save TriageData to
	 */
	public final void initApplication(final String pathToDataFile) {
		this.getAppData().setConfig("path", pathToDataFile);

		// load data from either local or remote (if connection is available)
		this.loadData();
	}
}