package ca.kenma.triage.container;

import java.io.*;

import android.os.AsyncTask;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * A container for the Data container and various methods to save and retrieve
 * from files for future use. Also has methods to save and load from a remote
 * server with Data instances saved in a MySQL database.
 * 
 *
 * 
 */
public abstract class Application<T extends Data> {
	protected T appData;
	protected final Class<T> dataType;
	protected boolean isConnected;

	/**
	 * Constructs a new instance of Application.
	 * 
	 * @param type
	 *            the class type of Data that this Application deals with
	 */
	public Application(Class<T> type) {
		this.dataType = type;
		try {
			this.appData = this.dataType.newInstance();
		} catch (Exception e) {
		}
	}

	/**
	 * Records if network connectivity is available.
	 * 
	 * @param isConnected
	 *            true if network is available, otherwise false
	 */
	public void setIsConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	/**
	 * Retrieves the Data container of this Application.
	 * 
	 * @return the Data container of this Application
	 */
	public T getAppData() {
		return this.appData;
	}

	/**
	 * Saves the Data of this Application to a external file named
	 * (getAppData().getConfig("path")) and creates it if it doesn't exist.
	 * "path" in the config is set by initApplication().
	 * 
	 * @return true iff Data was saved successfully else returns false
	 */
	public boolean saveData() {
		try {

			// tag last saved time
			this.appData.tagAsSaved();

			// initialize gson
			Gson gson = new Gson();

			// make a file object with the specified path recorded by
			// initApplication
			File appFile = new File((String) this.getAppData()
					.getConfig("path"));

			// check if file exists
			if (!appFile.exists()) {

				// create the file if it doesn't exist
				appFile.createNewFile();
			}

			// if network is available, try to save Data to remote server
			if (isConnected) {
				new AsyncSave().execute(gson.toJson(this.appData));
			}

			// write the Data in the appFile
			FileOutputStream out = new FileOutputStream(appFile);

			// serialize the entire Data instance to a string with the GSON
			// library
			out.write((gson.toJson(this.getAppData())).getBytes());
			out.close();

			// return true to indicate that data was saved
			return true;

		} catch (Exception e) {

			// returns false if Exception was thrown during saving file
			return false;
		}
	}

	/**
	 * Returns a string of the first line in a file in path. This is expected 
	 * to be a JSON string.
	 * 
	 * @return a JSON string to be used to instantiate a Data instance
	 */
	public boolean loadData() {

		// if network is available, try to load from remote server
		if (isConnected) {
			try {

				// toggle connection to load from local first (for merging
				// purposes)
				isConnected = false;
				loadData();

				// toggle back online
				isConnected = true;

				// get remote Data
				String remote = new AsyncLoad().execute(
						"http://kenma.ca/triage.php").get();

				if (loadData(remote)) {

					// if loading the remote JSON string worked then save it
					// locally to a file
					saveData();
				} else {

					// otherwise, tag network as offline (to prevent loading
					// from malformed JSON)
					isConnected = false;
				}
			} catch (Exception e) {

				// something broke, so take network as offline (to prevent
				// loading from problematic JSON)
				isConnected = false;
			}
		} else {

			// since we're offline, load only from local file containing JSON
			// string
			try {

				// load the local file
				File appFile = new File((String) this.getAppData().getConfig(
						"path"));
				Scanner scanner = new Scanner(new FileInputStream(appFile));
				String serializedData = scanner.nextLine();

				// if there was some JSON string returned by loadData(..), then
				// the user exists and we need to check the password.
				if (serializedData != null) {

					// instantiate the JSON string into a Data object
					return loadData(serializedData);
				}
			} catch (FileNotFoundException e) {

				// if file did not exist, then create a new file
				this.saveData();
			}
		}

		return false;
	}

	/**
	 * Generates a Data from a JSON String.
	 * 
	 * @param serializedData
	 *            a JSON string
	 * @return true if a Data was successfully generated, false otherwise
	 */
	public boolean loadData(String serializedData) {
		try {

			// start up GSON service
			Gson gson = new Gson();
			T newData = gson.fromJson(serializedData, this.dataType);

			// only merge if we're online
			if (isConnected) {

				// determine which data is newer by the last saved time
				if (newData.lastSaved() < appData.lastSaved()) {

					// if local is newer, then use local as overriding Data
					newData.merge(appData);
				} else {

					// otherwise, use remote as overriding Data
					appData.merge(newData);
					newData = appData;
				}
			}

			this.appData = newData;

			// since this operation worked without problems, report it as 
			// worked
			return true;
		} catch (Exception e) {

			// otherwise, it failed
			return false;
		}
	}

	/**
	 * Attempts to load a remote website and retrieves its entire HTML contents
	 * as a string asynchronously (to avoid bogging up the main thread). 
	 * Sourced from: 
	 * http://developer.android.com/reference/android/os/AsyncTask.html
	 * 
	 * @author makenne1
	 * 
	 */
	private class AsyncLoad extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {
			HttpResponse response = null;
			HttpGet httpGet = null;
			HttpClient mHttpClient = null;
			String s = "";

			try {
				if (mHttpClient == null) {

					// start up the querying service
					mHttpClient = new DefaultHttpClient();
				}

				// tag the url to get as the first parameter
				httpGet = new HttpGet(urls[0]);

				// perform the get action
				response = mHttpClient.execute(httpGet);
				s = EntityUtils.toString(response.getEntity(), "UTF-8");

			} catch (Exception e) {
			}

			// return the HTML retrieved from website
			return s;
		}
	}

	/**
	 * Attempts to send a JSON string remotely by POST-ing it asynchronously.
	 * Sourced from:
	 * http://developer.android.com/reference/android/os/AsyncTask.html
	 * 
	 * @author makenne1
	 * 
	 */
	private class AsyncSave extends AsyncTask<String, Integer, Double> {
		@Override
		protected Double doInBackground(String... params) {

			// call the actual saving method
			saveToServer(params[0]);
			return null;
		}
	}

	/**
	 * Attempts to POST a JSON string to http://kenma.ca/triage.php, which 
	 * saves it to a remote MySQL database. Sourced from:
	 * http://stackoverflow.com/questions
	 * /15851574/passing-string-array-to-php-as-post
	 * 
	 * @param json
	 *            the JSON string containing a Data instance
	 */
	private void saveToServer(String json) {

		// sends gson.toJson(this.getAppData); (a string) via POST to
		// http://kenma.ca/triage.php under POST key "data"
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://kenma.ca/triage.php");

		try {
			// Add your data
			List<BasicNameValuePair> nameValuePairs = 
					new ArrayList<BasicNameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("data", json));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
		} catch (Exception e) {
		}
	}
}