package ca.kenma.triage.container;

import java.util.HashMap;
import java.util.Map;

import ca.kenma.triage.model.EmergencyRoom;
import ca.kenma.triage.model.PatientDatabase;

/**
 * A container that stores all Application config(uration) settings, a database
 * of Patients and those Patients that have not been seen by a doctor yet. This
 * Data class is usually saved and loaded by an Application class which 
 * converts values to and from JSON strings for storage.
 * 
 *
 * 
 */
public class TriageData extends Data implements Accounts<Account> {

	private Map<String, Account> accounts = new HashMap<String, Account>();
	private PatientDatabase patients;
	private EmergencyRoom active;

	/**
	 * Constructs a new blank Data instance.
	 */
	public TriageData() {

		// sets the default sort order of the patient list menu (on the right)
		// to be by Arrival Time and not Urgency (since Urgency is not yet
		// implemented, but will be in Phase III)
		setConfig("sortByUrgency", false);
		this.patients = new PatientDatabase();
		this.active = new EmergencyRoom();
	}

	/**
	 * Returns the PatientDatabase of this Data. This is a collection of all
	 * Patients registered with the Application.
	 * 
	 * @return the PatientDatabase of this Data
	 */
	public PatientDatabase getPatients() {
		return patients;
	}

	/**
	 * Returns the EmergencyRoom patients of this Data. Emergency Room Patients
	 * are those that have not been seen by a doctor yet.
	 * 
	 * @return the EmergencyRoom patients
	 */
	public EmergencyRoom getActive() {
		return active;
	}

	/**
	 * Depreciated. activeAccount is not stored in config anymore. Please refer
	 * to the isLoggedIn() method in the containing Application class
	 * 
	 * @return false (not used anymore)
	 */
	public boolean isLoggedIn() {
		return false;
	}

	/**
	 * Retrieves the associated Account instance of the specified username if 
	 * it exists.
	 * 
	 * @param username
	 *            the username of the requested Account
	 * @return the Account instance of the username iff it exists, otherwise
	 *         null
	 */
	@Override
	public Account getAccount(String username) {
		try {

			// grab the account
			return this.accounts.get(username);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Retrieves the associated Account instance containing the combination of
	 * username and password if it exists. Usually used for login verification.
	 * 
	 * @param username
	 *            the username
	 * @param password
	 *            the password for the Account with username
	 * @return the Account instance with matching username and password
	 */
	@Override
	public Account getAccount(String username, String password) {
		Account account = this.getAccount(username);
		if (account == null) {

			// if account doesn't exist, then return null
			return null;
		} else {

			// if account exists, check if the passwork matches
			if (account.getPassword().equals(password)) {
				return account;
			} else {

				// if the password does not match, return null
				return null;
			}
		}
	}

	/**
	 * Registers a new Account instance with the Account database.
	 * 
	 * @param account
	 *            the Account instance to register
	 * @return true if the Account was added successfully, otherwise false
	 */
	@Override
	public boolean addAccount(Account account) {
		if (this.getAccount(account.getUsername()) == null) {

			// only if the account doesn't exist, then we add it
			this.accounts.put(account.getUsername(), account);
			return true;
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return accounts.isEmpty();
	}

	@Override
	public void merge(Data newTriageData) {

		// merge accounts
		accounts.putAll(((TriageData) newTriageData).accounts);

		// merge patientsdb
		patients.getDatabase().putAll(
				((TriageData) newTriageData).getPatients().getDatabase());

		// replace the entire eroom with the new version (no merging)
		active = ((TriageData) newTriageData).getActive();
	}
}