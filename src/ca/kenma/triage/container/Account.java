package ca.kenma.triage.container;

public class Account {
		protected String username;
		protected String password;
		protected String name;
		protected int usergroupId;

		/**
		 * Sets up this account with username, password, name, usergroupid
		 */
		public Account(String username, String password, String name,int usergroupId) {
			this.username = username;
			this.password = password;
			this.name = name;
			this.usergroupId = usergroupId;
		}

		/**
		 * Gets the username of this account.
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * Sets the username to be username to this account.
		 * 
		 * @param username
		 *            the username to be set on this account
		 */
		public void setUsername(String username) {
			this.username = username;
		}

		/**
		 * Gets the password of this account.
		 * 
		 * @return the password of this account.
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * Sets the password to be password to this account.
		 * 
		 * @param password
		 *            the password to be set to this account.
		 */
		public void setPassword(String password) {
			this.password = password;
		}

		/**
		 * Gets the name of this account.
		 * 
		 * @return the name of this account.
		 */
		public String getName() {
			return name;
		}

		/**
		 * Sets the name to be name to this account.
		 * 
		 * @param name
		 *            the name to be set to this account.
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Gets the usergroupId of this account.
		 * 
		 * @return the usergroupId of this account.
		 */
		public int getUsergroupId() {
			return usergroupId;
		}

		/**
		 * Sets the usergroupId of this account.
		 * 
		 * @param usergroupId
		 *            the usergroupId to be set to this account.
		 */
		public void setUsergroupId(int usergroupId) {
			this.usergroupId = usergroupId;
		}

		/**
		 * Returns the username of this account.
		 */
		public String toString() {
			return this.username;
		}
	}

