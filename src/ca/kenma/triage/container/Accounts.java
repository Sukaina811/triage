package ca.kenma.triage.container;

/**
 * Provides methods dealing with Account Authentication on Activities
 * and classes. Allows Accounts to be saved and retrieved.
 * 
 *
 *
 * @param <T> the type of Account that should be Authenticated with
 */
public interface Accounts<T extends Account> {
	/**
	 * Retrieves the Account that belongs to username or null if it does
	 * not exist.
	 * @param username the username that belongs to an Account to retrieve
	 * @return the Account with username or null if it does not exist
	 */
	public abstract T getAccount(String username);
	
	/**
	 * Retrieves the Account that has the specified combination of username
	 * and password or null if it does not exist. Generally used for login
	 * verification.
	 * @param username the username that belongs to an Account
	 * @param password the password for that Account
	 * @return the Account with matching username and password or null if it
	 * does not exist
	 */
	public abstract T getAccount(String username, String password);
	
	/**
	 * Registers a new Account to this Authentication if it does not already
	 * exist.
	 * @param account the Account to add
	 * @return true if the Account was added otherwise false
	 */
	public abstract boolean addAccount(T account);
}
