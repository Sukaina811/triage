package ca.kenma.triage.container;

/**
 * A interface used to give classes methods to deal with logging in, and
 * to deal with Accounts.
 * 
 *
 *
 */
public interface Authentication<T extends Account> {
	
	/**
	 * Marks an Account as currently logged in.
	 * @param account the Account that is logged in
	 */
	public abstract void setActiveAccount(T account);
	
	/**
	 * Retrieves the currently logged in Account if it exists.
	 * @return the logged in Account or null if no one is logged in
	 */
	public abstract T getActiveAccount();
	
	/**
	 * Determines if an Account is logged in or not.
	 * @return true if an Account has been set as active otherwise false
	 */
	public abstract boolean isLoggedIn();
}
