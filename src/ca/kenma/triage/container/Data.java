package ca.kenma.triage.container;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * A container that stores all Application config(uration) settings, This Data
 * class is usually saved and loaded by an Application class which converts
 * values to and from JSON strings for storage.
 * 
 *
 * 
 */
public abstract class Data {

	protected Map<String, Object> config = new HashMap<String, Object>();

	protected Long lastSaved = new Date().getTime();

	/**
	 * Retrieves the configuration setting assigned to the desired key.
	 * 
	 * @param key
	 *            the configuration name we want to retrieve
	 * @return the configuration setting value
	 */
	public Object getConfig(String key) {
		return config.get(key);
	}

	/**
	 * Stores a configuration setting for the Application.
	 * 
	 * @param key
	 *            the configuration name
	 * @param value
	 *            the actual configuration setting
	 */
	public void setConfig(String key, Object value) {
		config.put(key, value);
	}

	/**
	 * Determines if this Data was a newly created instance. Usually used to
	 * check if this Data was made at the start of an Application.
	 * 
	 * @return whether or not this Data is new
	 */
	public abstract boolean isEmpty();

	/**
	 * Takes another Data and updates this Data with its newer values.
	 * 
	 * @param data
	 *            another Data instance to merge with
	 */
	public abstract void merge(Data data);

	/**
	 * Records the last time this Data was saved at. Used to determine the
	 * method of merging.
	 */
	public void tagAsSaved() {
		lastSaved = new Date().getTime();
	}

	/**
	 * Retrieves the last time this Data was saved.
	 * 
	 * @return the time this last Data was saved at in long.
	 */
	public Long lastSaved() {
		return lastSaved;
	}
}