package ca.kenma.triage.utils;

import ca.kenma.triage.R;
import ca.kenma.triage.model.Patient;
import ca.kenma.triage.view.BaseActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Used to populate a ListView with icons, a label, and a Patient (if
 * specified). Used along with DrawerItem
 * 
 * Sourced from:
 * http://stackoverflow.com/questions/17069797/add-icons-to-android-
 * navigation-drawer
 * 
 *
 */
public class DrawerItemAdapter extends ArrayAdapter<DrawerItem> {

	private LayoutInflater inflater;
	private Context c;

	/**
	 * Creates a new DrawerItemAdapter.
	 * 
	 * @param context
	 *            the Context
	 * @param textViewResourceId
	 *            which TextView
	 * @param objects
	 *            a list of DrawerItems
	 */
	public DrawerItemAdapter(Context context, int textViewResourceId,
			DrawerItem[] objects) {
		super(context, textViewResourceId, objects);
		this.inflater = LayoutInflater.from(context);
		this.c = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DrawerItem drawerItem = this.getItem(position);
		return getItemView(convertView, parent, drawerItem);
	}

	/**
	 * Generates a View with data from DrawerItem populated into it.
	 * 
	 * @param convertView
	 *            a layout for each item
	 * @param parentView
	 *            the layout to place these items into
	 * @param drawerItem
	 *            the DrawerItem that contains information for each View
	 * @return a populated View from DrawerItem
	 */
	public View getItemView(View convertView, ViewGroup parentView,
			DrawerItem drawerItem) {

		DrawerItemHolder drawerItemHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.drawer_item, parentView,
					false);
			TextView labelView = (TextView) convertView
					.findViewById(R.id.drawer_item_label);
			TextView iconView = (TextView) convertView
					.findViewById(R.id.drawer_item_icon);

			drawerItemHolder = new DrawerItemHolder();
			drawerItemHolder.labelView = labelView;
			drawerItemHolder.iconView = iconView;

			convertView.setTag(drawerItemHolder);
		}

		if (drawerItemHolder == null) {
			drawerItemHolder = (DrawerItemHolder) convertView.getTag();
		}

		// set color coded icons based on Patient Urgency
		Patient patient = drawerItem.getPatient();
		if (patient != null) {
			int urgency = patient.getUrgencyLevel();

			// add the urgency level of the patient to the label
			drawerItem.setLabel(drawerItem.getLabel() + " (UL"
					+ ((Integer) urgency).toString() + ")");

			int urgencyColor;
			switch (urgency) {
			case (4):
				urgencyColor = R.color.red;
				break;
			case (3):
				urgencyColor = R.color.red;
				break;
			case (2):
				urgencyColor = R.color.yellow;
				break;
			default:
				urgencyColor = R.color.green;
				break;
			}

			drawerItemHolder.iconView.setTextColor(c.getResources().getColor(
					urgencyColor));
		}

		// set the fonts to our roboto and icon:fontawesome
		drawerItemHolder.labelView.setText(drawerItem.getLabel());
		drawerItemHolder.labelView.setTypeface(BaseActivity.getFont(c,
				"robotothinitalic.ttf"));
		drawerItemHolder.iconView.setText(drawerItem.getIcon());
		drawerItemHolder.iconView.setTypeface(BaseActivity.getFont(c,
				"fontawesome.otf"));

		return convertView;
	}

	@Override
	public boolean isEnabled(int position) {
		return getItem(position).isEnabled();
	}

	/**
	 * Holds both icons and labels temporarily.
	 */
	private static class DrawerItemHolder {
		private TextView labelView;
		private TextView iconView;
	}
}