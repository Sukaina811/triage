package ca.kenma.triage.utils;

import ca.kenma.triage.model.Patient;
import android.content.Context;

/**
 * This is an Android related class, sourced from:
 * http://www.michenux.net/android-navigation-drawer-748.html
 * 
 * It has been modified for our custom icons and app. This class helps create
 * items in our navigation menus.
 * 
 *
 */
public class DrawerItem {

	private int id;
	private String label;
	private Patient patient;
	private String icon;
	private String onclick;
	private boolean updateActionBarTitle;

	private DrawerItem() {
	}

	/**
	 * Creates a new DrawerItem instance for usage in DrawerItemAdapter.
	 * 
	 * @param id
	 *            an unique id for this DrawerItem
	 * @param label
	 *            a description of what this DrawerItem is used for and to be
	 *            displayed in the ListView
	 * @param icon
	 *            an id for an icon for this DrawerItem found in strings.xml
	 * @param updateActionBarTitle
	 *            unused at this time (specify false)
	 * @param context
	 *            the Context
	 * @return a DrawerItem to be used in DrawerItemAdapter
	 */
	public static DrawerItem create(int id, String label, int icon,
			boolean updateActionBarTitle, Context context) {

		// set all attributes of this DrawerItem
		DrawerItem item = new DrawerItem();
		item.setId(id);
		item.setLabel(label);
		item.setPatient(null);
		item.setIcon(icon, context);
		item.setUpdateActionBarTitle(updateActionBarTitle);
		return item;
	}

	/**
	 * Creates a new DrawerItem with a Patient class to be used in
	 * DrawerItemAdapter (when used to make a list of Patients).
	 * 
	 * @param id
	 *            an unique id for this DrawerItem
	 * @param patient
	 *            the Patient that this DrawerItem is associated with
	 * @param label
	 *            a description of what this DrawerItem is used for and to be
	 *            displayed in the ListView
	 * @param icon
	 *            an id for an icon for this DrawerItem found in strings.xml
	 * @param updateActionBarTitle
	 *            unused at this time (specify false)
	 * @param context
	 *            the Context
	 * @return a DrawerItem to be used in DrawerItemAdapter
	 */
	public static DrawerItem create(int id, Patient patient, int icon,
			boolean updateActionBarTitle, Context context) {

		// set attributes of this DrawerItem
		DrawerItem item = new DrawerItem();
		item.setId(id);
		item.setLabel(patient.getName());
		item.setPatient(patient);
		item.setIcon(icon, context);
		item.setUpdateActionBarTitle(updateActionBarTitle);
		return item;
	}

	/**
	 * Gets the ID of this DrawerItem.
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the ID for this DrawerItem.
	 * 
	 * @param id
	 *            the id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the sting label of this DrawerItem.
	 * 
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Gets the Patient associated with this DrawerItem.
	 * 
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * Sets the label for this DrawerItem.
	 * 
	 * @param label
	 *            the label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Associates a Patient with this DrawerItem.
	 * 
	 * @param patient
	 *            the Patient to associate with
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * Gets the icon string for this DrawerItem.
	 * 
	 * @return a icon string
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon for this DrawerItem.
	 * 
	 * @param icon
	 *            the id of the icon desired (from strings.xml)
	 * @param context
	 *            the Context
	 */
	public void setIcon(int icon, Context context) {
		this.icon = (String) context.getResources().getString(icon);
	}

	/**
	 * Gets the clicked action of this DrawerItem.
	 * 
	 * @return the action
	 */
	public String getOnClick() {
		return onclick;
	}

	/**
	 * Sets what to do with this DrawerItem when clicked.
	 * 
	 * @param intent
	 *            the action method
	 */
	public void setOnClick(String intent) {
		this.onclick = intent;
	}

	/**
	 * Checks if this DrawerItem is enabled.
	 * 
	 * @return true
	 */
	public boolean isEnabled() {
		return true;
	}

	/**
	 * Does nothing right now.
	 * 
	 * @return if it should update the Action Bar
	 */
	public boolean updateActionBarTitle() {
		return this.updateActionBarTitle;
	}

	/**
	 * Does nothing right now.
	 * 
	 * @param updateActionBarTitle
	 *            whether or not to update the Action Bar
	 */
	public void setUpdateActionBarTitle(boolean updateActionBarTitle) {
		this.updateActionBarTitle = updateActionBarTitle;
	}
}