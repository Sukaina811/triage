package ca.kenma.triage.model;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient>{

	@Override
	public int compare(Patient patientOne, Patient patientTwo) {
		return patientTwo.getUrgencyLevel() - patientOne.getUrgencyLevel();
	}

}
