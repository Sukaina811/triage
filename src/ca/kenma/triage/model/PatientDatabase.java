package ca.kenma.triage.model;


import java.util.Map;
import java.util.HashMap;
import java.util.List;

/**
 * A representation of a PatientDatabase.
 * 
 *
 *
 */
public class PatientDatabase {
	
	private Map<String, Patient> database;
	
	/**
	 * Creates a new PatienDatabase.
	 */
	public PatientDatabase() {
		this.database = new HashMap<String, Patient>();
	}
	
	/**
	 * Adds a Patient with its id to this PatientDatabase.
	 * @param id the id of the Patient to be added to this PatientDatabase.
	 * @param patient the Patient to be added to this PatientDatabase.
	 */
	public void addPatient(String id, Patient patient){
		this.database.put(id, patient);		
	}
	
	/**
	 * Return the Patient with HealthCardNumber id.
	 * @param id health card number of the Patient in this Database.
	 * @return Patient with health card number id.
	 */
	public Patient getPatient(String id){
		
		for (Patient patient : database.values()) {
			if (patient.getHealthCardNumber().equals(id)) {
				return patient;
			}
		}
		return null;
	}
    
	/**
	 * Returns the Patient in this PatientDatabase whose id is id.
	 * @param id the id of the Patient in this PatienDatabase to be returned.
	 * @return the Patient with the id in this PatienDatabase.
	 */
	public List<PatientRecord> getPatientRecord(String id){
		return this.database.get(id).getRecord();
	}
	
	/**
	 * Returns the Patient database.
	 * @return the Patient database.
	 */
	public Map<String, Patient> getDatabase() {
		return database;
	}
	
	
}
