package ca.kenma.triage.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import android.content.Context;
import ca.kenma.triage.R;
import ca.kenma.triage.utils.DrawerItem;

/**
 * A representation of an Emergency Room.
 * 
 *
 * 
 */
public class EmergencyRoom {

	// the list of patients
	private List<String> patientList = new ArrayList<String>();

	// creates a comparator to compare the patients
	PatientComparator patientCompare = new PatientComparator();

	// a unique ID to every patient (used when populating the right drawer)
	private static int patientID;

	/**
	 * Adds a new patient to this emergency room.
	 * 
	 * @param patient
	 *            The patient that is added to this emergency room.
	 */
	public void addPatient(Patient patient) {

		// adds the patient's health card number to patient list
		patientList.add(patient.getHealthCardNumber());
	}

	/**
	 * Returns a patient list sorted by arrival time.
	 * 
	 * @param patientsDB
	 *            the patient database.
	 * @return a patient list sorted by arrival time.
	 */
	public List<Patient> getPatientListByTime(PatientDatabase patientsDB) {

		// create a new list of patients
		List<Patient> patients = new ArrayList<Patient>();

		// populate that list with patients using their health card numbers
		for (String patientNumber : patientList) {
			patients.add(this.getPatient(patientNumber, patientsDB));
		}

		// return the list of patients
		return patients;
	}

	/**
	 * Returns a patient list sorted by urgency level.
	 * 
	 * @param patientsDB
	 *            the patient database.
	 * @return a patient list sorted by urgency level.
	 */
	public List<Patient> getPatientListByUrgency(PatientDatabase patientsDB) {

		// creates a new patient list that will be sorted by urgency level
		ArrayList<Patient> patientListByUrgency = new ArrayList<Patient>(
				patientList.size());

		// gets the list of patients
		List<Patient> patients = this.getPatientListByTime(patientsDB);

		// populate the cloned list with patients
		for (Patient patient : patients) {
			patientListByUrgency.add(patient);
		}

		// sorts the patient list by descending urgency level
		Collections.sort(patientListByUrgency, patientCompare);

		// return the sorted list
		return patientListByUrgency;
	}

	/**
	 * Removes a patient from this Emergency Room.
	 * 
	 * @param patientID
	 *            The patient ID of the patient to be removed.
	 */
	public void removePatient(String patientID) {

		// remove patient from list
		for (Iterator<String> it = patientList.iterator(); it.hasNext();) {
			String patient = it.next();
			if (patient.equals(patientID)) {
				it.remove();
			}
		}
	}

	/**
	 * Returns a patient of this emergency room.
	 * 
	 * @param patientID
	 *            the health card number of the patient.
	 * @param patientsDB
	 *            the patient database.
	 * @return a patient of this emergency room.
	 */
	public Patient getPatient(String patientID, PatientDatabase patientsDB) {
		return patientsDB.getPatient(patientID);
	}

	/**
	 * Returns an array of patient DrawerItems sorted by Urgency Level or
	 * Arrival Time.
	 * 
	 * @param c
	 *            The context of the form using this method.
	 * @param byUrgency
	 *            If true, sort the patients by Urgency Level otherwise sort by
	 *            arrival time.
	 * 
	 * @param patientsDB
	 *            the patient database.
	 * 
	 * @return An array of DrawerItems used to populate drawers.
	 */
	public DrawerItem[] toDrawerItems(Context c, boolean byUrgency,
			PatientDatabase patientsDB) {

		// creates a drawer item array that is used to populate
		// the right drawer (drawer of patients currently in the Emergency Room)
		DrawerItem[] drawablePatientList = new DrawerItem[patientList.size()];

		// a new patient list to populate the array with
		List<Patient> categorizedList = new ArrayList<Patient>();

		// sort the list according to urgency or arrival time
		if (byUrgency) {
			categorizedList = getPatientListByUrgency(patientsDB);

		} else {
			categorizedList = getPatientListByTime(patientsDB);
		}

		// create the drawer item
		for (int i = 0; i < categorizedList.size(); i++) {
			drawablePatientList[i] = DrawerItem.create(patientID,
					categorizedList.get(i), R.string.icon_male, false, c);
			patientID += 1;
		}

		// return the array to populate the right drawer
		return drawablePatientList;
	}
}