package ca.kenma.triage.model;

public class Prescription implements Record {
	
	// name of the medicine
	private String medName;
	// information about the medicine
	private String medInfo;
	// time this Prescription was added to Patient Record
	private TCalendar timeNoted;
	
	/**
	 * Creates a new Prescription with medicine medName and information
	 * medInfo about the medicine and notes the time
	 * @param medName name of the medicine in this Prescription
	 * @param medInfo information about medicine in this Prescription
	 */
	public Prescription(String medName, String medInfo) {
		this.medName = medName;
		this.medInfo = medInfo;
		this.timeNoted = new TCalendar();
	}
	
	/**
	 * Returns the name of the medicine of this Prescription
	 * @return the name of the medicine of this Prescription
	 */
	public String getMedName() {
		return medName;
	}
	
	/**
	 * Returns the information about the medicine of this Prescription
	 * @return the information about the medicine of this Prescription
	 */
	public String getMedInfo() {
		return medInfo;
	}
	
	/**
	 * Return the time this Prescription was noted
	 * @return the time this Prescription was noted
	 */
	public TCalendar getTimeNoted(){
		return this.timeNoted;
	}
	
	/**
	 * Returns the String representation of this Prescription
	 * 
	 * @return the String representation of this Prescription
	 */
	public String recordToString() {
		
		// returns a string with this prescriptions information
		return "Date: " + this.getTimeNoted().getDate() + "	" + "\n" +
				"Time: " + this.getTimeNoted().getTime() + "\n" +
				"Medication Name: " + this.medName + "\n" +
				"Medication Info: " + this.medInfo;
	}
	
}