package ca.kenma.triage.model;

import java.util.Calendar;

public class TCalendar implements Record {

	// the Calendar object of this TCalendar
	private Calendar tcalendar = Calendar.getInstance();

	/**
	 * Creates a new TCalendar object with no parameters
	 */
	public TCalendar() {
		// super();
	}

	/**
	 * Returns the integer representing the day of the month for this TCalendar
	 * 
	 * @return integer for day of the month
	 */
	public int getDay() {
		return tcalendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Returns the integer representing the month for this TCalendar
	 * 
	 * @return integer for month
	 */
	public int getMonth() {
		return tcalendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * Returns the integer representing the year for this TCalendar
	 * 
	 * @return integer for year
	 */
	public int getYear() {
		return tcalendar.get(Calendar.YEAR);
	}

	/**
	 * Returns the integer representing the hour (12 hour format) for this
	 * calendar
	 * 
	 * @return hour based on 12 hour format
	 */
	public int getHour() {
		return tcalendar.get(Calendar.HOUR);
	}

	/**
	 * Returns the integer representing the minute for this TCalendar
	 * 
	 * @return minute for this TCalendar
	 */
	public int getMinute() {
		return tcalendar.get(Calendar.MINUTE);
	}

	/**
	 * Returns string "AM" or "PM" based on the time of the day for this
	 * TCalendar
	 * 
	 * @return string "AM" or "PM" based on the time of the day for this
	 *         TCalendar
	 */
	public String getAM_PM() {
		if (this.tcalendar.get(Calendar.AM_PM) == 0) {
			return "AM";
		} else {
			return "PM";
		}
	}

	/**
	 * Returns date in DD/MM/YYYY format for this TCalendar
	 * 
	 * @return date in DD/MM/YYYY format for this TCalendar
	 */
	public String getDate() {
		String daystr = new Integer(this.getDay()).toString();
		String monthstr = new Integer(this.getMonth()).toString();
		String yearstr = new Integer(this.getYear()).toString();

		String day = "00".substring(daystr.length()) + daystr;
		String month = "00".substring(monthstr.length()) + monthstr;
		String year = "0000".substring(yearstr.length()) + yearstr;
		return day + "/" + month + "/" + year;
	}

	/**
	 * Returns time in hh:mm format followed by "AM" or "PM"
	 * 
	 * @return time in hh:mm format followed by "AM" or "PM"
	 */
	public String getTime() {
		// get hour and minute for this TCalendar
		Integer hr = this.getHour();
		// for 12 hour(AM-PM) format we change 0hr to 12AM
		if (hr == 0) {
			hr = 12;
		}
		Integer min = this.getMinute();
		String hrstr = hr.toString();
		String minstr = min.toString();
		// format hour to make it to format hh
		String hour = "00".substring(hrstr.length()) + hrstr;
		// format minute to make it of format mm
		String minute = "00".substring(minstr.length()) + minstr;

		return hour + ":" + minute + this.getAM_PM();
	}

	/**
	 * Returns the number of years passed from this TCaledar's date till the
	 * present
	 * 
	 * @return the number of years passed from this TCalendar's date till the
	 *         present
	 */
	public int getYearsPassed() {
		// gets the current day and time
		Calendar now = Calendar.getInstance();

		// value we have to return
		int yearsPassed = 0;

		// Compare the current month and this tcalendar's month, and subtract 1
		// if tcalendar's month is greater than current month
		if (this.tcalendar.get(Calendar.MONTH) > now.get(Calendar.MONTH)) {
			yearsPassed -= 1;
		}

		// Compare the days of the month
		// if tcalendar's month is in current month and tcalendar's day of the
		// month is less than the current day of the month then subtract 1 from
		// yearsPassed
		else if ((this.tcalendar.get(Calendar.MONTH) == now.get(Calendar.MONTH))
				&& (this.tcalendar.get(Calendar.DAY_OF_MONTH) > now
						.get(Calendar.DAY_OF_MONTH))) {
			yearsPassed -= 1;
		}

		// add the difference between current year and tcalendar's year to
		// yearsPassed
		return yearsPassed += (now.get(Calendar.YEAR) - this.tcalendar
				.get(Calendar.YEAR));
	}

	/**
	 * Returns the instance of Calendar that this TCalendar holds
	 * 
	 * @return the instance of Calendar for this TCalendar
	 */
	public Calendar getCalendar() {
		// returns the instance of Calendar that this TCalendar holds
		return this.tcalendar;
	}

	/**
	 * Sets the date of this TCalendar to the given day, month and year
	 * 
	 * @param day
	 *            day of the date of this TCalendar
	 * @param month
	 *            month of the date of this TCalendar
	 * @param year
	 *            Year of the date of this TCalendar
	 */
	public void setDate(int day, int month, int year) {
		this.tcalendar.set(year, month - 1, day);
	}

	/**
	 * Sets the Calendar of this TCalendar to cal
	 * 
	 * @param cal the new value of tcalendar
	 */
	public void setCalendar(Calendar cal) {
		this.tcalendar = cal;
	}

	/**
	 * Compares the given TCalendar with this TCalendar and returns 1 iff this
	 * TCalendar's date and time are after the given TCalendar
	 * 
	 * @param tcal
	 *            the TCalendar to compare time with this
	 * @return 1 iff this TCalendar's date and time are after the given
	 *         TCalendar and -1 otherwise
	 */
	public int compareTo(TCalendar tcal) {
		return this.tcalendar.compareTo(tcal.getCalendar());
	}
	
	/**
	 * Return the String representation of this TCalendar for time seen
	 * by Doctor
	 * 
	 * @return String representation of the time seen by physician
	 */
	public String recordToString() {
		return "Date: " + this.getDate() + "\nSeen by Physician at "
				+ this.getTime();
	}
	
	/**
	 * Returns itelf
	 * @return returns itself
	 */
	public TCalendar getTimeNoted() {
		return this;
	}

}