package ca.kenma.triage.model;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/**
 * A representation of a Patient.
 * 
 *
 * 
 */
public class Patient implements Serializable {

	private static final long serialVersionUID = -4470404399443798702L;
	private String name;
	private TCalendar birthdate;
	private String healthCardNumber;
	private int urgencyLevel;
	private List<PatientRecord> records = new ArrayList<PatientRecord>();

	/**
	 * Creates a new Patient with given name, birth Calendar and health card
	 * number.
	 * 
	 * @param name
	 *            name of this Patient.
	 * @param birthdate
	 *            birth date of this Patient.
	 * @param healthCardNo
	 *            health card number of this Patient.
	 */
	public Patient(String name, TCalendar birthdate, String healthCardNumber) {
		this.name = name;
		this.birthdate = birthdate;
		this.healthCardNumber = healthCardNumber;
		this.addRecord();
	}

	/**
	 * Returns the name of this Patient.
	 * 
	 * @return the name of this Patient.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of this Patient to name.
	 * 
	 * @param name
	 *            sets the name of this patient to name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the Health Card number of this Patient.
	 * 
	 * @return the Health Card number of this Patient.
	 */
	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	/**
	 * Return the arrival time of this Patient.
	 * 
	 * @return the arrival time of this Patient.
	 */
	public TCalendar getArrivalTime() {
		return this.records.get(records.size() - 1).getArrivalTime();
	}

	/**
	 * Returns the Urgency Level of this Patient.
	 * 
	 * @return the Urgency Level of this Patient.
	 */
	public int getUrgencyLevel() {

		// returns the urgency level
		return this.urgencyLevel;
	}

	/**
	 * Returns the birthday of this Patient in dd/mm/yyyy format.
	 * 
	 * @return the birthday of this Patient in dd/mm/yyyy format.
	 */
	public String getBirthdate() {
		return this.birthdate.getDate();
	}

	/**
	 * Set the birthdate of this Patient to birthday.
	 * 
	 * @param birthday
	 *            birthdate of this Patient.
	 */
	public void setBirthdate(TCalendar birthday) {
		this.birthdate = birthday;
	}

	/**
	 * Calculates the Urgency Level of this Patient based on the latest.
	 * VitalSigns of this Patient.
	 */
	public void calcUrgencyLevel() {

		if (this.getLatestRecord().getLatestVitalSigns() != null) {
			// get latest vitalSigns from the latest PatientRecord
			VitalSigns latestvitals = this.getLatestRecord()
					.getLatestVitalSigns();
			// initialize urgency and add 1 to urgency if it this Patient's
			// age is below 2 or latest vitalSigns are critical
			int urgency = 0;
			if (this.getAge() < 2) {
				urgency += 1;
			}

			if (latestvitals.getTemperature() >= 39.0) {
				urgency += 1;
			}
			if ((latestvitals.getBloodPressure()[0] >= 140)
					|| (latestvitals.getBloodPressure()[1] >= 90)) {
				urgency += 1;
			}
			if ((latestvitals.getHeartRate() >= 100)
					|| (latestvitals.getHeartRate() <= 50)) {
				urgency += 1;
			}
			this.urgencyLevel = urgency;
		}
	}

	/**
	 * Returns the records of this Patient.
	 * 
	 * @return the records of this Patient.
	 */
	public List<PatientRecord> getRecord() {
		return this.records;
	}

	/**
	 * Return the age of this Patient.
	 * 
	 * @return the age of this Patient.
	 */
	public int getAge() {
		// call the getYearsPassed() method of TCalendar which returns
		// the year difference from a date and present date
		return this.birthdate.getYearsPassed();
	}

	/**
	 * Adds a new PatientRecord of this Patient.
	 */
	public void addRecord() {
		// add a new PatientRecord Object to list of records
		this.records.add(new PatientRecord());

		// reset the urgency level
		this.urgencyLevel = 0;

		// add 1 to urgency level if this Patient's age is less than 2
		if (this.getAge() < 2) {
			this.urgencyLevel += 1;
		}
	}

	/**
	 * Returns the latest record of this Patient.
	 * 
	 * @return the latest Record of this Patient.
	 */
	public PatientRecord getLatestRecord() {
		// returns the last element of records
		return this.records.get(this.records.size() - 1);
	}

}
