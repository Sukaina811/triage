package ca.kenma.triage.model;

public interface Record {
	
	/**
	 * Returns the time this Record was noted
	 * @return the time this Record was noted
	 */
	public TCalendar getTimeNoted();
	
	/**
	 * Returns this record's string representation
	 * @return this record's string representation
	 */
	public String recordToString();
}