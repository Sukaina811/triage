package ca.kenma.triage.model;

/**
 * A representation of Vital Signs.
 * 
 *
 * 
 */
public class VitalSigns implements Record {

	private int[] bloodPressure;
	private double temperature;
	private int heartRate;
	private TCalendar timeNoted;

	/**
	 * Constructor for this VitalSigns object.
	 * 
	 * @param bloodPressure
	 *            the blood pressure of the patient.
	 * @param temperature
	 *            the temperature of the patient.
	 * @param heartRate
	 *            the heart rate of the patient.
	 */
	public VitalSigns(int[] bloodPressure, double temperature, int heartRate) {
		//super();
		this.bloodPressure = bloodPressure;
		this.temperature = temperature;
		this.heartRate = heartRate;
		this.timeNoted = new TCalendar();
	}
	

	/**
	 * Returns an integer array of [systolic, diastolic] blood pressure of this
	 * vital sign.
	 * 
	 * @return an array of [systolic, diastolic] blood pressure of this vital
	 *         sign.
	 */
	public int[] getBloodPressure() {
		return this.bloodPressure;
	}

	/**
	 * Returns the temperature of this vital sign.
	 * 
	 * @return the temperature of this vital sign.
	 */
	public double getTemperature() {
		return this.temperature;
	}

	/**
	 * Returns the heart rate of this vital sign.
	 * 
	 * @return the heart rate of this vital sign.
	 */
	public int getHeartRate() {
		return this.heartRate;
	}

	public TCalendar getTimeNoted() {
		return this.timeNoted;
	}

	/**
	 * Returns a string representation of this VitalSigns
	 * 
	 * @return a string representation of this VitalSigns
	 */
	public String recordToString() {
		
		Integer bps = this.getBloodPressure()[0];
		Integer bpd = this.getBloodPressure()[1];
		Integer hr = this.getHeartRate();
		Double temp = this.getTemperature();
		return "Date: " + this.getTimeNoted().getDate() + "	" + "Time: " + 
				this.getTimeNoted().getTime() + "\n" + 
				"BP Systolic: " + bps + "\n" + 
				"BP Dystolic: " + bpd + "\n" + 
				"Heart Rate: " + hr + "\n" + 
				"Temperature: " + temp;
	}
}