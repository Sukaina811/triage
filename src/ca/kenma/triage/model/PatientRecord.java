package ca.kenma.triage.model;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;

import ca.kenma.triage.model.VitalSigns;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;

/**
 * A representation of a PatientRecord class.
 * 
 *
 * 
 */
public class PatientRecord implements Serializable {

	// a string array contains the string representation of each day in a week
	private final static String[] DAYS = new String[] { "Sunday", "Monday",
			"Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

	// the unique id for serialization
	private static final long serialVersionUID = 8852389780285976171L;

	// a map correspond each time to a symptom instance
	private Map<Long, String> symptoms;

	// an array of VitalSigns for this PatientRecord
	private List<VitalSigns> vitalSigns;

	// an array of Prescriptions for this PatientRecord
	private List<Prescription> prescriptions;

	// an array list of seen by doctor time
	private List<TCalendar> timeSeenByDoctor = new ArrayList<TCalendar>();

	// the arrivalTime variable
	private TCalendar arrivalTime;

	// the latest VitalSign
	private VitalSigns latestVitalSigns = null;

	/**
	 * Creates a new PatientRecord.
	 */
	public PatientRecord() {
		this.vitalSigns = new ArrayList<VitalSigns>();
		this.prescriptions = new ArrayList<Prescription>();
		this.symptoms = new HashMap<Long, String>();
		this.arrivalTime = new TCalendar();
	}

	/**
	 * Adds a vitalsign to this PatientRecord.
	 * 
	 * @param vitalsign
	 *            the vitalsign to be added to this PatientRecord.
	 */
	public void addVitalSigns(VitalSigns vitalsign) {
		this.vitalSigns.add(vitalsign);
		this.latestVitalSigns = vitalsign;
	}

	/**
	 * Adds a new Prescription to this PatientRecord
	 * 
	 * @param medName
	 *            name of the medicine
	 * @param medInfo
	 *            information about the medicine
	 */
	public void addPrescription(Prescription prescription) {
		this.prescriptions.add(prescription);
	}

	/**
	 * Adds a symptom to this PatientRecord.
	 * 
	 * @param symptom
	 *            the symptom that is added to this PatientRecord.
	 */
	public void addSymptoms(String symptom) {
		symptoms.put(Calendar.getInstance().getTimeInMillis(), symptom);
	}

	/**
	 * Adds a time which is the time the patient to be seen by doctor to this
	 * PatientRecord.
	 */
	public void addTimeSeenByDoctor() {
		TCalendar timeSeen = new TCalendar();
		this.timeSeenByDoctor.add(timeSeen);
	}

	/**
	 * Returns the Arrival time of Patient for this PatientRecord.
	 * 
	 * @return the Arrival time of Patient for this PatientRecord.
	 */
	public TCalendar getArrivalTime() {
		return this.arrivalTime;
	}

	/**
	 * Returns a string represents the arrival time of this PatientRecord's
	 * arrival time info.
	 * 
	 * @return a string represents the arrival time of this PatientRecord's
	 *         arrival time info.
	 */
	public String getArrivalTimeString() {

		// returns a string arrival time
		return "Arrival Time: " + this.arrivalTime.getTime() + " ("
				+ DAYS[arrivalTime.getCalendar().get(Calendar.DAY_OF_WEEK) - 1]
				+ ")";
	}

	/**
	 * Returns the latest VitalSigns of this PatientRecord.
	 * 
	 * @return the latest VitalSigns of this PatientRecord.
	 */
	public VitalSigns getLatestVitalSigns() {
		return this.latestVitalSigns;
	}

	public List<Record> getRecords() {
		// list of all vitalsigns, prescription and times seen by doctor
		List<Record> record = new ArrayList<Record>();
		// index of the last element in vitalSigns
		int vs = this.vitalSigns.size() - 1;
		// index of the last element in prescriptions
		int ps = this.prescriptions.size() - 1;
		// index of the last element in timeSeenByDoctor
		int ts = this.timeSeenByDoctor.size() - 1;

		// loop till all the elements in vitalSigns, prescriptions,
		// timeSeenByDoctor are added to record
		while ((vs > -1) || (ps > -1) || (ts > -1)) {
			// to decide whether any vitalSigns[vs] is added to record
			boolean flag1 = true;
			// to decide whether prescription[ps] is added to record
			boolean flag2 = true;

			// to compare elements of vitalSigns from the end of list
			if ((vs != -1)) {

				// to decide whether to add vitalSigns[vs] to record
				boolean psbool = true;
				if (ps != -1) {
					psbool = (this.vitalSigns
							.get(vs)
							.getTimeNoted()
							.compareTo(
									this.prescriptions.get(ps).
									getTimeNoted()) == 1);
				}

				// to decide whether to add vitalSigns[vs] to record
				boolean tsbool = true;
				if (ts != -1) {
					tsbool = (this.vitalSigns
							.get(vs)
							.getTimeNoted()
							.compareTo(
									this.timeSeenByDoctor.get(ts)
											.getTimeNoted()) == 1);
				}

				// add vitalSigns[vs] to record only iff psbool and tsbool are
				// true
				if (psbool && tsbool) {
					record.add(this.vitalSigns.get(vs));
					// move vs to next index of vitalSigns
					vs = vs - 1;
					// set flag1 to false to decide to start the loop all over
					flag1 = false;
				}
			}

			// move ahead only if nothing is added from vitalSigns is added
			// to record and to compare elements of prescriptions from the
			// end of list
			if (flag1 && (ps != -1)) {
				// to decide whether to add prescriptions[ps] to record
				boolean tsbool = true;
				if (ts != -1) {
					tsbool = (this.prescriptions
							.get(ps)
							.getTimeNoted()
							.compareTo(
									this.timeSeenByDoctor.get(ts)
											.getTimeNoted()) == 1);
				}

				// add prescription[ps] to record iff tsbool is true
				if (tsbool) {
					record.add(prescriptions.get(ps));
					// move ps to next index of prescriptions
					ps = ps - 1;
					// set flag2 to false to decide to start the loop all over
					flag2 = false;
				}
			}

			// move ahead only if nothing is added from vitalSigns and
			// prescriptions is added to record and to add element from
			// timeSeenByDoctor from the end of the list
			if (flag1 && flag2 && (ts != -1)) {
				record.add(timeSeenByDoctor.get(ts));
				// move ts to next index of timeSeenByDoctor
				ts = ts - 1;
			}
		}
		return record;
	}

	/**
	 * Return an array of string representations of all the records (Vital
	 * Signs, Prescriptions, Time Seen by Doctor) sorted by time
	 * 
	 * @return an array of string representations of all the records
	 */
	public String[] getAllRecords() {
		// get all records (Vital Signs, Prescriptions, Time Seen by Doctor)
		List<Record> record = this.getRecords();
		// String array that we have to return
		String[] recordList = new String[record.size()];

		// add string representations of all records to recordList
		for (int i = 0; i < record.size(); i++) {
			recordList[i] = new String(record.get(i).recordToString());
		}
		return recordList;
	}

}