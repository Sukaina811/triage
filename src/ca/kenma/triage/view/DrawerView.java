package ca.kenma.triage.view;

import android.widget.ListView;
import android.content.Context;
import android.util.AttributeSet;

/**
 * Extends the basic built-in ListView class. Allows the usage of custom List
 * items (DrawerItem).
 * 
 * 
 * 
 */
public class DrawerView extends ListView {

	/**
	 * Constructions a new DrawerView. Generally used when this DrawerView is
	 * built programmatically in Java.
	 * 
	 * @param context
	 *            the Context of the Layout
	 */
	public DrawerView(Context context) {
		super(context);
	}

	/**
	 * Constructs a new DrawerView. Generally used when this DrawerView is 
	 * built in a XML layout.
	 * 
	 * @param context
	 *            the Context of the Layout
	 * @param attrs
	 *            attributes of this DrawerView
	 */
	public DrawerView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Constructs a new DrawerView. Generally used when this DrawerView is 
	 * build in a XML Layout.
	 * 
	 * @param context
	 *            the Context of the Layout
	 * @param attrs
	 *            attributes of this DrawerView
	 * @param defStyle
	 *            an id of a style XML file to stylize this DrawerView
	 */
	public DrawerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}