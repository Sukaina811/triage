package ca.kenma.triage.view;

import ca.kenma.triage.R;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A Get Patient by ID activity form that lets the user enter a health card
 * number to add a patient or find a patient.
 * 
 * 
 * 
 */
public class GetPatientIDActivity extends BaseActivity {
	Intent intent;

	@Override
	public boolean isAllowed() {
		return (this.isLoggedIn());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout) findViewById(
				R.id.content_frame);
		LayoutInflater inflater = (LayoutInflater) getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout patientIDForm = (RelativeLayout) inflater.inflate(
				R.layout.patientid_form, null);
		RelativeLayout formLayout = (RelativeLayout) inflater.inflate(
				R.layout.form, null);
		((RelativeLayout) formLayout.findViewById(R.id.form_content))
				.addView(patientIDForm);

		contentFrame.addView(formLayout);

		// change fonts and sets text of headers and descriptions
		TextView header = (TextView) findViewById(R.id.form_header_title);
		header.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		header.setText(this.getResources().getString(
				R.string.patient_id_form_title));

		TextView desc = (TextView) findViewById(R.id.form_header_text);
		desc.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		desc.setText(this.getResources().getString(
				R.string.patient_id_form_desc));

		intent = getIntent();
	}
	
	/**
	 * Finds/Adds patient with a specified health card number.
	 * 
	 * @param view The view of this activity.
	 */
	public void patientIdSubmit(View view) {

		// the full map of the values passed on into this activity
		Bundle b = intent.getExtras();

		// get the text entered into the health card number field
		EditText healthCardNumberField = (EditText) findViewById(
				R.id.patientid_id);
		String healthCardNumber = healthCardNumberField.getText().toString();

		// if the health card field is not blank
		if (!healthCardNumber.equals("")) {

			// Add Patient:
			// if the extra "add" is true, then we want to add a new patient
			// object, otherwise we just want to show the patient info
			if ((Boolean) b.get("add")) {

				Intent intent = new Intent(this, AddPatientActivity.class);

				// passing the patients health card number to
				// add patient activity
				intent.putExtra("healthCardNumber", healthCardNumber);
				this.startActivity(intent);

				// Find Patient:
				// show the patient information
			} else {

				// if the patient database contains a patient
				// with that health card number
				if (BaseActivity.application.getAppData().getPatients()
						.getDatabase().containsKey(healthCardNumber)) {

					Intent intent = new Intent(this, 
							PatientViewActivity.class);

					// passing the patients health card number to
					// patient view activity
					intent.putExtra("healthCardNumber", healthCardNumber);
					this.startActivity(intent);

					// if no patient in the database has this health card 
					// number
				} else {

					// display a toast to warn the user
					Toast.makeText(
							this,
							this.getResources().getString(
									R.string.toast_failed_DNE_ID_message),
							Toast.LENGTH_LONG).show();
				}
			}

			// if the health card number field is empty
		} else {

			// display a toast to warn the user
			Toast.makeText(
					this,
					this.getResources().getString(
							R.string.toast_failed_empty_ID_message),
					Toast.LENGTH_LONG).show();
		}
	}
}
