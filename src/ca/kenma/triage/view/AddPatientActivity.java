package ca.kenma.triage.view;

import java.util.Calendar;

import ca.kenma.triage.R;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import ca.kenma.triage.model.*;

/**
 * A add patient activity form to let the user add new patients.
 * 
 *
 * 
 */
public class AddPatientActivity extends BaseActivity {

	private String healthCardNumber = "";
	private Integer year;
	private Integer month;
	private Integer day;
	public String[] birth;
	private TCalendar birthday;
	private boolean isExists = false;

	@Override
	public boolean isAllowed() {
		return (this.isLoggedIn() && BaseActivity.application
				.getActiveAccount().getUsergroupId() == 1);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// if user is not a Physician then go ahead
		if (this.isAllowed()) {
			
			// inflate xml so that we can manipulate the layout
			FrameLayout contentFrame = (FrameLayout) findViewById(
					R.id.content_frame);
			LayoutInflater inflater = (LayoutInflater) getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			RelativeLayout patientAddForm = (RelativeLayout) inflater.inflate(
					R.layout.patientadd_form, null);
			RelativeLayout formLayout = (RelativeLayout) inflater.inflate(
					R.layout.form, null);
			((RelativeLayout) formLayout.findViewById(R.id.form_content))
					.addView(patientAddForm);

			contentFrame.addView(formLayout);

			// change fonts and sets text of headers and descriptions
			TextView header = (TextView) findViewById(R.id.form_header_title);
			header.setTypeface(BaseActivity.getFont(this,
					"robotothinitalic.ttf"));
			header.setText(this.getResources().getString(
					R.string.patient_form_title));

			TextView desc = (TextView) findViewById(R.id.form_header_text);
			desc.setTypeface(BaseActivity.
					getFont(this, "robotothinitalic.ttf"));
			desc.setText(this.getResources().getString(
					R.string.patient_form_desc));

			// get the intent that directs us here and then get the
			// healthCardNumber that passed in
			Intent intent = getIntent();
			String id = (String) intent
					.getSerializableExtra("healthCardNumber");

			// set the healthCardNumber to be the id we got
			healthCardNumber = id;

			// if the healthCardNumber exists in our database
			if (BaseActivity.application.getAppData().getPatients()
					.getDatabase().containsKey(id)) {

				// set the fields that will populate to be the correct info
				// that we could retrieve by the healthCardNumber
				EditText dob = (EditText) findViewById(R.id.patient_dob);
				dob.setText(BaseActivity.application.getAppData().getPatients()
						.getPatient(id).getBirthdate());

				EditText name = (EditText) findViewById(R.id.patient_name);
				name.setText(BaseActivity.application.getAppData()
						.getPatients().getPatient(id).getName());

				// set a boolean variable isExists to true indicate the
				// healthCardNumbe exists already
				isExists = true;
			}
		} else {
			
			// go back to the previous page
			Toast.makeText(this, "You are not authorized to add Patients.",
					Toast.LENGTH_LONG).show();
			Intent i = new Intent(this, MainActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
		}
	}
	
	/**
	 * Adds a patient to the database/emergency room.
	 * 
	 * @param view The view of this activity.
	 */
	public void patientAddSubmit(View view) {

		// this is where we finally add the patient
		// find the dob and name shown in the dob and name field(possibly
		// changed)
		EditText dobField = (EditText) findViewById(R.id.patient_dob);
		String dob = dobField.getText().toString();

		EditText nameField = (EditText) findViewById(R.id.patient_name);
		String name = nameField.getText().toString();

		// initiate a isSuccessful to be true for later use
		boolean isSuccessful = true;

		// here we check if any of the field is empty, if not a toast msg
		// indicate
		// failure(in the last else statement)
		if ((!dob.equals("")) && (!name.equals(""))) {

			// try to create a calendar(birthday) by using the
			// info the user entered into the field
			try {

				birth = dob.split("/");
				day = Integer.parseInt(birth[0]);
				month = Integer.parseInt(birth[1]);
				year = Integer.parseInt(birth[2]);
				birthday = new TCalendar();
				birthday.setDate(day, month, year);
				Calendar now = Calendar.getInstance();

				// make sure the format of dob is valid and what we expect
				if ((birth.length != 3) || (year.toString().length() > 4)
						|| (now.compareTo(birthday.getCalendar()) == -1)) {
					
					// not successful in entering birthdate
					isSuccessful = false;
				}

				// not successful in entering birthdate
			} catch (Exception e) {

				isSuccessful = false;
			}

			// do the add(update) patient step here only
			// if it is successful
			if (isSuccessful) {

				// new patient added, user did not save at this point
				BaseActivity.isSaved = false;

				// if our patient doesn't exist
				if (!isExists) {

					// create it(in the mean time) create a record for him
					Patient patient = new Patient(name, birthday,
							healthCardNumber);
					BaseActivity.application.getAppData().getPatients()
							.addPatient(healthCardNumber, patient);
					BaseActivity.application.getAppData().getActive()
							.addPatient(patient);
					patient.addRecord();
					Intent intent = new Intent(this, 
							PatientViewActivity.class);

					// pass the healthCardNumber to PatientViewActivity
					// and start it
					intent.putExtra("healthCardNumber", healthCardNumber);
					this.startActivity(intent);

				} else {

					Patient patient = BaseActivity.application.getAppData()
							.getPatients().getPatient(healthCardNumber);
					
					// if it exists, we do an update on patients personal info
					patient.setBirthdate(birthday);
					patient.setName(name);

					// only go to PatienViewActivity if the patient
					// is not in emergency room now
					if (!BaseActivity.application
							.getAppData()
							.getActive()
							.getPatientListByTime(
									BaseActivity.application.getAppData()
											.getPatients()).contains(patient)) 
					{
						
						// add a new record to the patient
						patient.addRecord();
						
						// add the patient to the emergency room
						application.getAppData().getActive()
								.addPatient(patient);
						
						// create a new intent to go
						// to the PatientViewActivity
						Intent intent = new Intent(this,
								PatientViewActivity.class);
						
						// pass in the patient's health card number
						intent.putExtra("healthCardNumber", healthCardNumber);
						
						// start PatientViewActivity
						this.startActivity(intent);

					} else {

						// if already in emergency room, a toast message
						// warning the user
						Toast.makeText(
								this,
								this.getResources().getString(
										R.string.toast_warning_already_in_ER),
								Toast.LENGTH_LONG).show();
						
						// create a new intent to go
						// to the AddPatientActivity
						Intent intent = new Intent(AddPatientActivity.this,
								GetPatientIDActivity.class);
						
						// pass in true for add since we are adding
						// a new patient
						intent.putExtra("add", true);
						
						// start AddPatientActivity
						AddPatientActivity.this.startActivity(intent);
					}
				}

			} else {

				// a toast message indicating an invalid date of birth
				Toast.makeText(
						this,
						this.getResources().getString(
								R.string.toast_failed_invalid_dob),
						Toast.LENGTH_LONG).show();
			}

		} else {

			// a toast message indicating empty fields
			Toast.makeText(
					this,
					this.getResources().getString(
							R.string.toast_failed_empty_fields),
					Toast.LENGTH_LONG).show();
		}
	}
}
