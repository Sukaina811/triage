package ca.kenma.triage.view;

import ca.kenma.triage.R;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import ca.kenma.triage.model.*;

/**
 * A add vitalsigns form to let the user to enter the patient's medical
 * information.
 * 
 *
 * 
 */
public class AddVitalsActivity extends BaseActivity {

	private String healthCardNumber;
	private Bundle extras;

	@Override
	public boolean isAllowed() {
		return (this.isLoggedIn());
	}

	/**
	 * Initiate this activity when its start up on screen.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout)
				findViewById(R.id.content_frame);
		LayoutInflater inflater = (LayoutInflater)
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout patientAddForm = (RelativeLayout) inflater.inflate(
				R.layout.vitalsadd_form, null);
		RelativeLayout formLayout = (RelativeLayout) inflater.inflate(
				R.layout.form, null);
		((RelativeLayout) formLayout.findViewById(R.id.form_content))
				.addView(patientAddForm);

		contentFrame.addView(formLayout);

		// change fonts and sets text of headers and descriptions
		TextView header = (TextView) findViewById(R.id.form_header_title);
		header.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		header.setText(this.getResources().getString(
				R.string.title_activity_vitals_add));

		TextView desc = (TextView) findViewById(R.id.form_header_text);
		desc.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		desc.setText(this.getResources()
				.getString(R.string.vitalsign_form_desc));

		extras = getIntent().getExtras();
	}
	
	/**
	 * Adds a vital sign to the patient.
	 * 
	 * @param view The view of this activity.
	 */
	public void vitalsAddSubmit(View view) {

		// get the healthCardNumber passed in from patientViewActivity
		healthCardNumber = (String) extras.get("healthCardNumber");

		// use the fields to find this patient's various medical information
		EditText heartrateField = (EditText)
				findViewById(R.id.vitals_heartrate);
		String heartrate = heartrateField.getText().toString();

		EditText bpsField = (EditText)
				findViewById(R.id.vitals_bloodpressure_systolic);
		String bps = bpsField.getText().toString();

		EditText bpdField = (EditText)
				findViewById(R.id.vitals_bloodpressure_diastolic);
		String bpd = bpdField.getText().toString();

		EditText temperatureField = (EditText)
				findViewById(R.id.vitals_temperature);
		String temperature = temperatureField.getText().toString();

		// a boolean to check if the vital sign was successful or not
		boolean isSuccessful = true;

		// add a vital sign only if all fields are entered by the user
		if (!heartrate.equals("") && !bps.equals("") && !bpd.equals("")
				&& !temperature.equals("")) {

			try {

				// creates a new vital sign for the patient
				VitalSigns vitalsign = new VitalSigns(new int[] {
						Integer.parseInt(bps), Integer.parseInt(bpd) },
						Double.parseDouble(temperature),
						Integer.parseInt(heartrate));

				Patient patientToEditER = BaseActivity.application
						.getAppData()
						.getActive()
						.getPatient(
								healthCardNumber,
								BaseActivity.application.getAppData()
										.getPatients());

				patientToEditER.getLatestRecord().addVitalSigns(vitalsign);
				patientToEditER.calcUrgencyLevel();

				Intent intent = new Intent(this, PatientViewActivity.class);
				intent.putExtra("healthCardNumber", healthCardNumber);

				// display a toast message to tell the user vitals were added
				Toast.makeText(
						this,
						this.getResources()
								.getString(
										R.string
										.toast_successful_addvitals_vitals_added),
						Toast.LENGTH_LONG).show();

				// new vital sign added, user did not save at this point
				BaseActivity.isSaved = false;

				this.startActivity(intent);

			} catch (Exception e) {

				// failed to add vital sign to patient
				isSuccessful = false;
			}

			// if failed to add vital sign
			if (!isSuccessful) {

				// display a toast message to warn the user
				Toast.makeText(
						this,
						this.getResources().getString(
								R.string.toast_failed_addvitals_invalid_fields),
						Toast.LENGTH_LONG).show();
			}

		} else {

			// display a toast message to tell the user to enter all fields
			Toast.makeText(
					this,
					this.getResources().getString(
							R.string.toast_failed_addvitals_empty_fields),
					Toast.LENGTH_LONG).show();
		}
	}
}
