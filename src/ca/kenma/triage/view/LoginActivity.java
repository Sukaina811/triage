package ca.kenma.triage.view;

import ca.kenma.triage.R;
import ca.kenma.triage.container.Account;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A login activity form to let the user log in.
 * 
 * 
 * 
 */
public class LoginActivity extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout) this
				.findViewById(R.id.content_frame);
		LayoutInflater inflater = (LayoutInflater) getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout accountForm = (RelativeLayout) inflater.inflate(
				R.layout.account_form, null);
		RelativeLayout formLayout = (RelativeLayout) inflater.inflate(
				R.layout.form, null);
		((RelativeLayout) formLayout.findViewById(R.id.form_content))
				.addView(accountForm);

		contentFrame.addView(formLayout);

		// change fonts and sets text of headers and descriptions
		TextView header = (TextView) findViewById(R.id.form_header_title);
		header.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		header.setText(this.getResources().getString(R.string.account_login));

		TextView desc = (TextView) findViewById(R.id.form_header_text);
		desc.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		desc.setText(this.getResources().getString(
				R.string.account_login_desc));
	}
	
	/**
	 * Attempt to login.
	 * 
	 * @param view The view of this activity.
	 */
	public void accountSubmit(View view) {

		// get the username entered by the user
		EditText usernameText = (EditText) findViewById(R.id.account_username);
		String username = usernameText.getText().toString();

		// get the password entered by the user
		EditText passwordText = (EditText) findViewById(R.id.account_password);
		String password = passwordText.getText().toString();

		// only login a new account if both of the fields are not empty
		if (!username.equals("") && !password.equals("")) {

			Account account = BaseActivity.application.getAccount(username,
					password);

			// if login is successful
			if (account != null) {

				BaseActivity.application.setActiveAccount(account);

				// set save variable to true (user changed nothing at this
				// point)
				BaseActivity.isSaved = true;

				// a toast message indicating a successful login
				Toast.makeText(
						this,
						this.getResources().getString(
								R.string.toast_successful_login_message),
						Toast.LENGTH_LONG).show();

				Intent intent = new Intent(this, MainActivity.class);

				// clear top activities
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				// start main activity (dashboard)
				startActivity(intent);

				// if fail to login
			} else {
				// clears the password field
				passwordText.setText("");

				// a toast message indicating a failed login
				Toast.makeText(
						this,
						this.getResources().getString(
								R.string.toast_failed_login_message),
						Toast.LENGTH_LONG).show();
			}

			// if one of the fields are empty
		} else {

			// a toast message indicating an empty field
			Toast.makeText(
					this,
					this.getResources().getString(
							R.string.toast_failed_login_empty_fields),
					Toast.LENGTH_LONG).show();
		}
	}

}