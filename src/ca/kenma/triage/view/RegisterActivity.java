package ca.kenma.triage.view;

import ca.kenma.triage.R;
import ca.kenma.triage.container.Account;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A register activity form that lets the user register a new account.
 * 
 *
 * 
 */
public class RegisterActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout) findViewById(
				R.id.content_frame);
		LayoutInflater inflater = (LayoutInflater) getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout accountForm = (RelativeLayout) inflater.inflate(
				R.layout.account_form, null);
		RelativeLayout accountFormRegister = (RelativeLayout) inflater.inflate(
				R.layout.account_form_register, null);
		Button accountButton = (Button) accountForm
				.findViewById(R.id.account_button);
		accountButton.setText(getResources().getString(
				R.string.account_register));
		RelativeLayout formLayout = (RelativeLayout) inflater.inflate(
				R.layout.form, null);
		((RelativeLayout) formLayout.findViewById(R.id.form_content))
				.addView(accountForm);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ABOVE, R.id.account_username);
		((RelativeLayout) formLayout.findViewById(R.id.account_form)).addView(
				accountFormRegister, params);
		contentFrame.addView(formLayout);

		// change fonts and sets text of headers and descriptions
		TextView header = (TextView) findViewById(R.id.form_header_title);
		header.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		header.setText(this.getResources().getString(
				R.string.account_register));

		TextView desc = (TextView) findViewById(R.id.form_header_text);
		desc.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		desc.setText(this.getResources().getString(
				R.string.account_register_desc));
	}
	
	/**
	 * Registers a new user.
	 * 
	 * @param view The view of this activity.
	 */
	public void accountSubmit(View view) {

		Intent intent = new Intent(this, MainActivity.class);

		// gets the username entered by the user
		EditText userNameText = (EditText) findViewById(R.id.account_username);
		String username = userNameText.getText().toString();

		// gets the password entered by the user
		EditText passwordText = (EditText) findViewById(R.id.account_password);
		String password = passwordText.getText().toString();

		// gets the type of user
		RadioButton nurse = (RadioButton) findViewById(R.id.radioNurse);
		RadioButton physician = (RadioButton) findViewById(
				R.id.radioPhysician);

		// only register a new account if both of the fields are not empty
		if (!username.equals("") && !password.equals("")
				&& (nurse.isChecked() || physician.isChecked())) {

			// gets the type of user
			int userGroupId = 0;

			if (nurse.isChecked()) {
				userGroupId = 1;
			} else if (physician.isChecked()) {
				userGroupId = 2;
			}

			// if successfully registered show a toast message
			// and show the dashboard
			Account account = new Account(username, password,
					"DEPRECIATED: Full names are not used anymore.",
					userGroupId);

			if (BaseActivity.application.addAccount(account)) {
				Toast.makeText(
						this,
						this.getResources().getString(
								R.string.toast_successful_register_message),
						Toast.LENGTH_LONG).show();

				BaseActivity.application.setActiveAccount(account);

				Log.i("account registered", BaseActivity.application
						.getActiveAccount().getUsername());

				// auto saves information
				BaseActivity.application.saveData();
				BaseActivity.isSaved = true;

				// clear top activities
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				startActivity(intent);

				// if failed to register clear the fields and
				// show an error dialog
			} else {
				// sets the username and password fields to empty strings
				userNameText.setText("");
				passwordText.setText("");

				// shows a popup message indicating a failed register attempt
				AlertDialog.Builder errorMessage = 
						new AlertDialog.Builder(this);

				errorMessage.setTitle(this.getResources().getString(
						R.string.account_errorbox_title));

				errorMessage.setMessage(this.getResources().getString(
						R.string.account_errorbox_failed_register_message));

				errorMessage.setPositiveButton("OK", null);

				errorMessage.create().show();
			}

			// if the fields are empty then show a toast message
		} else {
			Toast.makeText(
					this,
					this.getResources().getString(
							R.string.toast_failed_register_empty_fields),
					Toast.LENGTH_LONG).show();
		}
	}

}
