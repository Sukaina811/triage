package ca.kenma.triage.view;

import ca.kenma.triage.R;
import ca.kenma.triage.model.*;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A patient view activity form that lets the user see information about the
 * patient.
 * 
 *
 * 
 */
public class PatientViewActivity extends BaseActivity {
	private Intent addVitals;
	private Intent addPrescription;

	private String healthCardNumber;
	private Patient patient;

	@Override
	public boolean isAllowed() {
		return (this.isLoggedIn());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout) findViewById(
				R.id.content_frame);
		LayoutInflater inflater = (LayoutInflater) getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout patientView = (RelativeLayout) inflater.inflate(
				R.layout.activity_patientview, null);
		contentFrame.addView(patientView);

		// get Patient's healthCardNumber from the intent
		Intent intent = getIntent();
		Bundle b = intent.getExtras();
		healthCardNumber = (String) b.get("healthCardNumber");

		// get Patient from PatientDatabase
		patient = application.getAppData().getPatients()
				.getPatient(healthCardNumber);

		// get Patient's name, date of birth and arrival time
		String name = patient.getName();
		String dob = patient.getBirthdate();
		String arrivalTime = patient.getLatestRecord().getArrivalTimeString();

		// populate the Record ListView
		ListView records = (ListView) findViewById(R.id.recordsList);
		ArrayAdapter<String> recordAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, patient.getLatestRecord()
						.getAllRecords());
		records.setAdapter(recordAdapter);

		// get the TextView objects patientview_name, patientview_dob,
		// patientview_id and patientview_healthCardNumber and set them to the
		// corresponding values for this Patient
		TextView patient_name = (TextView) findViewById(R.id.patientview_name);
		TextView patient_dob = (TextView) findViewById(R.id.patientview_dob);
		TextView patient_healthCardNumber = (TextView) findViewById(
				R.id.patientview_id);
		TextView patient_arrival = (TextView) findViewById(
				R.id.patientview_arrival);
		patient_name.setText(name);
		patient_dob.setText("Date of Birth (DD/MM/YYYY): " + dob);
		patient_arrival.setText(arrivalTime);
		patient_healthCardNumber.setText("Health Card Number: "
				+ healthCardNumber);

		// change fonts and sets text of headers and descriptions
		((TextView) findViewById(R.id.patientview_name))
				.setTypeface(BaseActivity.getFont(this, 
						"robotothinitalic.ttf"));
		((TextView) findViewById(R.id.patientview_dob))
				.setTypeface(BaseActivity.getFont(this, 
						"robotothinitalic.ttf"));
		((TextView) findViewById(R.id.patientview_id)).setTypeface(BaseActivity
				.getFont(this, "robotothinitalic.ttf"));
		((TextView) findViewById(R.id.patientview_arrival))
				.setTypeface(BaseActivity.getFont(this, 
						"robotothinitalic.ttf"));
		((TextView) findViewById(R.id.patientview_addvitals))
				.setTypeface(BaseActivity.getFont(this, "fontawesome.otf"));
		((TextView) findViewById(R.id.patientview_addprescription))
				.setTypeface(BaseActivity.getFont(this, "fontawesome.otf"));
		((TextView) findViewById(R.id.patientview_checkout))
				.setTypeface(BaseActivity.getFont(this, "fontawesome.otf"));

		// set the icon to move to AddVitalsActivity
		/*
		 * REMOVED FROM PHASE III LinearLayout rightTitleIcons = (LinearLayout)
		 * findViewById(R.id.title_right_icons); BaseActivity.addIcon(this,
		 * R.string.icon_edit, rightTitleIcons, new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * // Display message to tell user feature will be // implemented in
		 * phase 3 of this project Toast.makeText( PatientViewActivity.this,
		 * PatientViewActivity.this .getResources() .getString(
		 * R.string.toast_implemented_in_P3), Toast.LENGTH_LONG).show(); } });
		 */

		// set addVitals to intent to go to AddVitalsActivity
		addVitals = new Intent(this, AddVitalsActivity.class);
		// put healthCardNumber to retrieve information of Patient
		addVitals.putExtra("healthCardNumber", healthCardNumber);

		// set addPrescription to intent to go to AddPrescriptionActivity
		addPrescription = new Intent(this, AddPrescriptionActivity.class);
		// put healthCardNumber to retrieve information of Patient
		addPrescription.putExtra("healthCardNumber", healthCardNumber);
	}

	/**
	 * Starts AddVitalsActivity
	 * 
	 * @param v
	 *            view of current activity
	 */
	public void addVitals(View v) {
		if (BaseActivity.application.getActiveAccount().getUsergroupId() == 1) 
		{
			startActivity(addVitals);
		} else {
			Toast.makeText(this, "You are not authorized to add Vitals.",
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Starts AddPrescriptionActivity
	 * 
	 * @param v
	 *            view of current activity
	 */
	public void addPrescription(View v) {
		if (BaseActivity.application.getActiveAccount().getUsergroupId() == 2) 
		{
			startActivity(addPrescription);
		} else {
			Toast.makeText(this, "You are not authorized to add Prescription.",
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Checkouts the patient
	 * 
	 * @param v
	 *            view of current activity
	 */
	public void checkoutPatient(View v) {

		if (BaseActivity.application.getActiveAccount().getUsergroupId() == 1) 
		{
			// create a messagebox that asks the user if they
			// are sure they want to checkout the patient
			AlertDialog.Builder checkoutDialog = new AlertDialog.Builder(this);
			checkoutDialog.setMessage(this.getResources().getString(
					R.string.messagebox_check_checkout));

			checkoutDialog
					.setPositiveButton("Yes", checkoutDialogClickListener);

			checkoutDialog.setNegativeButton("No", 
					checkoutDialogClickListener);

			// show the messagebox
			checkoutDialog.show();

		} else {
			Toast.makeText(this,
					"You are not authorized to check out a patient.",
					Toast.LENGTH_LONG).show();
		}
	}

	DialogInterface.OnClickListener checkoutDialogClickListener = 
			new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {

			// if 'yes' is pressed
			case DialogInterface.BUTTON_POSITIVE:

				// mark patient's time seen by physician/doctor
				patient.getLatestRecord().addTimeSeenByDoctor();

				// remove patient from emergency room (ie, checkout)
				application.getAppData().getActive()
						.removePatient(patient.getHealthCardNumber());

				// create an intent that goes back to main activity (dashboard)
				Intent intent = new Intent(PatientViewActivity.this,
						MainActivity.class);

				// clear all previous pages
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				// patient checked out, user did not save at this point
				BaseActivity.isSaved = false;

				// start the intent
				startActivity(intent);

				// inform the user that the user has been checked out
				Toast.makeText(
						PatientViewActivity.this,
						PatientViewActivity.this.getResources().getString(
								R.string.messagebox_check_checkout_notif),
						Toast.LENGTH_LONG).show();

				break;

			// if 'no' is pressed
			case DialogInterface.BUTTON_NEGATIVE:

				// closes messagebox
				dialog.dismiss();
				break;
			}
		}
	};

}
