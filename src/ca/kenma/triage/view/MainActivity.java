package ca.kenma.triage.view;

import ca.kenma.triage.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * A main activity form, this is the main page (dashboard) of the application.
 * 
 * 
 * 
 */
public class MainActivity extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout) this
				.findViewById(R.id.content_frame);
		FrameLayout contentContainer = (FrameLayout) this
				.findViewById(R.id.content_container);
		DrawerLayout appFrame = (DrawerLayout) this
				.findViewById(R.id.app_frame);

		// add splash title
		LayoutInflater inflater = (LayoutInflater) getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout splash = (RelativeLayout) inflater.inflate(
				R.layout.main_splash_logo, null);
		contentFrame.addView(splash);

		TextView splashTitle = (TextView) findViewById(R.id.splash_title);
		TextView splashText = (TextView) findViewById(R.id.splash_text);

		// set the fonts for the splash screen
		splashTitle.setTypeface(BaseActivity.getFont(this,
				"robotothinitalic.ttf"));
		splashText.setTypeface(BaseActivity.getFont(this,
				"robotothinitalic.ttf"));

		// set background
		appFrame.setBackgroundResource(R.drawable.background_blank);
		contentContainer.setBackgroundResource(R.color.translucent);

	}

	/**
	 * Sourced from:
	 * http://indyvision.net/2010/02/android-request-user-decision-
	 * when-back-button-pressed/
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// if the back button is pressed
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			// ask the user if they want to quit
			AlertDialog.Builder quitDialog = new AlertDialog.Builder(this);
			quitDialog.setMessage(this.getResources().getString(
					R.string.toast_check_quit));
			quitDialog.setPositiveButton("Yes", quitDialogClickListener);
			quitDialog.setNegativeButton("No", quitDialogClickListener);
			quitDialog.show();
		}
		return false;
	}

	DialogInterface.OnClickListener quitDialogClickListener = 
			new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {

			// if the user presses yes
			case DialogInterface.BUTTON_POSITIVE:

				// close the activity
				finish();
				break;

			// if the user presses no
			case DialogInterface.BUTTON_NEGATIVE:

				// dismiss the messagebox
				dialog.dismiss();
				break;
			}
		}
	};
}
