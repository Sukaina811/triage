package ca.kenma.triage.view;

import ca.kenma.triage.container.*;

import java.util.Hashtable;

import ca.kenma.triage.R;
import ca.kenma.triage.model.Patient;
import ca.kenma.triage.utils.DrawerItem;
import ca.kenma.triage.utils.DrawerItemAdapter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class is used for the underlying base layout for all Activities in
 * Triage that needs the two side menus. It is (mostly) modular and can be
 * reused to create new Activities without redrawing the menus and other common
 * GUI elements.
 * 
 * 
 */
public abstract class BaseActivity extends Activity implements
		Accounts<Account>, Authentication<Account> {

	private DrawerItem[] drawerViewItems;
	private DrawerView leftDrawerView;
	private DrawerView rightDrawerView;
	private LinearLayout leftTitleIcons;
	private LinearLayout rightTitleIcons;

	/**
	 * A static Application instance that stores all information that is
	 * accessed by other classes that requires access to Data (like Patients).
	 * 
	 * This is static because there is only a single Application instance for
	 * all Activites, rather than a new (and different) instance for each
	 * Activity.
	 */
	public static TriageApplication application = new TriageApplication();

	/**
	 * A variable to store if the user has saved their Data or not.
	 */
	public static boolean isSaved = true;

	/**
	 * A variable to check if the Application has been loaded yet.
	 */
	public static boolean isLoaded = false;

	private boolean isUrgencySortOrder;

	/**
	 * Checks if the currently logged in Account is allowed to see this 
	 * Activity or not.
	 * 
	 * @return true if the Account is allowed here, false otherwise
	 */
	public boolean isAllowed() {
		return true;
	}

	/**
	 * this is the font cache. idea taken from:
	 * https://code.google.com/p/android/issues/detail?id=9904#c3
	 */
	protected static final Hashtable<String, Typeface> fontCache = 
			new Hashtable<String, Typeface>();

	/**
	 * Retrieves a Typeface object if it exists in the fontCache, or appends it
	 * if it doesn't. This is primarily used to avoid memory leak issues.
	 * 
	 * Taken from public domain:
	 * https://code.google.com/p/android/issues/detail?id=9904#c3
	 * 
	 * @param c
	 *            the context (Activity, View, etc)
	 * @param name
	 *            the filename of the font found in assets/
	 * @return the font object
	 */
	public static Typeface getFont(Context c, String name) {
		synchronized (fontCache) {
			if (!fontCache.containsKey(name)) {
				Typeface t = Typeface.createFromAsset(c.getAssets(), name);
				fontCache.put(name, t);
			}
			return fontCache.get(name);
		}
	}

	/**
	 * Adds an icon to the custom title bar.
	 * 
	 * @param c
	 *            the Context
	 * @param icon
	 *            the icon string id (from strings.xml)
	 * @param target
	 *            which LinearLayout we want to place the icon into
	 * @param listener
	 *            what to do when the icon is clicked
	 */
	public static void addIcon(Context c, int icon, LinearLayout target,
			OnClickListener listener) {

		// start up the inflater service
		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// takes our icon layout and inflates it into a Java object so that
		// we can start manipulating it programmatically
		TextView newIcon = (TextView) inflater.inflate(R.layout.title_icon,
				null);

		// change the text to weird characters used with "fontawesome"
		newIcon.setText(c.getResources().getString(icon));
		newIcon.setTypeface(BaseActivity.getFont(c, "fontawesome.otf"));

		// add a action when the icon is clicked
		newIcon.setOnClickListener(listener);

		// finally add to target title bar side
		if (target.getId() == R.id.title_left_icons) {
			target.addView(newIcon);
		} else {
			target.addView(newIcon, 0);
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		// check if Account logged in is allowed here each time the activity
		// is resumed
		if (!isAllowed()) {
			this.finish();
			Toast.makeText(BaseActivity.this,
					"You are not authorized to view this page.",
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Initializes the Activity when it's started up on screen.
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (!BaseActivity.isLoaded) {

			// check if network is available (for saving and loading)
			BaseActivity.application.setIsConnected(this.isNetworkAvailable());

			// start up the Application instance
			BaseActivity.application.initApplication(this.getFilesDir()
					+ "/appdata.txt");

			// record that we have done this init step (so we don't do it
			// again)
			BaseActivity.isLoaded = true;
		}

		super.onCreate(savedInstanceState);

		if (application.getAppData().getConfig("isUrgencySortOrder") != null) {
			isUrgencySortOrder = (Boolean) application.getAppData().getConfig(
					"isUrgencySortOrder");
		}

		// since we built our own title bar (because the title bar
		// can only be modified in API 11+), we hide the native
		// bar from view
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_base);

		// create the left drawer
		leftDrawerView = (DrawerView) findViewById(R.id.left_drawer);

		// creates the save data(login) item if logged in(out)
		DrawerItem loginItem = ((isLoggedIn()) ? DrawerItem.create(101,
				"Save Data", R.string.icon_save, false, this) : DrawerItem
				.create(101, "Login", R.string.icon_signin, false, this));

		// creates the logout(register) item if logged in(out)
		DrawerItem registerItem = ((isLoggedIn()) ? DrawerItem.create(101,
				"Logout", R.string.icon_signout, false, this)
				: DrawerItem.create(101, "Register", R.string.icon_folder_open,
						false, this));

		// creates the rest of the items
		drawerViewItems = new DrawerItem[] {
				DrawerItem.create(100, "Dashboard", R.string.icon_dashboard,
						false, this),
				loginItem,
				registerItem,
				DrawerItem.create(103, "Add Patient", R.string.icon_plus_sign,
						false, this),
				DrawerItem.create(105, "Find Patient", R.string.icon_search,
						false, this),
				DrawerItem.create(106, "Order by Urgency",
						R.string.icon_sort_by_attributes_alt, false, this),
				DrawerItem.create(107, "Order by Arrival Time",
						R.string.icon_sort_by_order, false, this) };

		// gives the left drawer click functionality
		leftDrawerView.setAdapter(new DrawerItemAdapter(this,
				R.layout.drawer_item, drawerViewItems));
		leftDrawerView.setOnItemClickListener(new LeftDrawerClickListener());

		// title bar mods
		leftTitleIcons = (LinearLayout) findViewById(R.id.title_left_icons);

		// button to open left drawer
		BaseActivity.addIcon(this, R.string.icon_reorder, leftTitleIcons,
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						openLeftDrawer(BaseActivity.this);
					}
				});

		// unlock Right Drawer only if its a nurse
		if (this.isLoggedIn()
				&& BaseActivity.application.getActiveAccount().
				getUsergroupId() == 1) {

			// unlock the right drawer
			unlockRightDrawer();

			// create the right drawer
			rightDrawerView = (DrawerView) findViewById(R.id.right_drawer);

			if (isUrgencySortOrder) {
				// gets the right drawers items (patients) from emergency room
				drawerViewItems = application
						.getAppData()
						.getActive()
						.toDrawerItems(this, true,
								application.getAppData().getPatients());
			} else {
				// gets the right drawers items (patients) from emergency room
				drawerViewItems = application
						.getAppData()
						.getActive()
						.toDrawerItems(this, false,
								application.getAppData().getPatients());
			}

			// gives the right drawer click functionality
			rightDrawerView.setAdapter(new DrawerItemAdapter(this,
					R.layout.drawer_item, drawerViewItems));
			rightDrawerView
					.setOnItemClickListener(new RightDrawerClickListener());

			// title bar mods
			rightTitleIcons = (LinearLayout) findViewById(
					R.id.title_right_icons);

			// button to open right drawer
			BaseActivity.addIcon(this, R.string.icon_group, rightTitleIcons,
					new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							openRightDrawer(BaseActivity.this);
						}
					});

		} else {

			// lock the right drawer
			lockRightDrawer();
		}

		// sets the title and font
		TextView titleBar = (TextView) findViewById(R.id.title_text);
		titleBar.setTypeface(BaseActivity.getFont(this, 
				"robotothinitalic.ttf"));
	}

	/**
	 * Provides click functionality of the right drawer.
	 * 
	 * @author makenne1
	 * 
	 */
	private class RightDrawerClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			Intent intent = new Intent(BaseActivity.this,
					PatientViewActivity.class);

			// gets an instance of the drawer item that was clicked
			DrawerItem drawerButton = (DrawerItem) parent
					.getItemAtPosition(position);

			// gets the patient from drawer item
			Patient patient = drawerButton.getPatient();

			// put the patient health card number into intent to pass onto the
			// next form
			intent.putExtra("healthCardNumber", patient.getHealthCardNumber());

			startActivity(intent);
		}
	}

	/**
	 * Provides click functionality of the left drawer.
	 * 
	 * @author makenne1
	 * 
	 */
	private class LeftDrawerClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			Intent i;

			// check which drawer item was clicked
			switch (position) {

			// dashboard item
			case (0):

				Intent intent = new Intent(BaseActivity.this,
						MainActivity.class);

				// clear top activities
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				startActivity(intent);
				break;

			// login(save data) item
			case (1):

				// check if logged in
				if (isLoggedIn()) {

					// save data
					save();
				} else {

					// if not logged in:
					// direct user to LoginActivity
					startActivity(new Intent(BaseActivity.this,
							LoginActivity.class));
				}
				break;

			// register(logout) item
			case (2):
				// check if logged in
				if (isLoggedIn()) {

					// if not saved, warn the user
					if (!isSaved) {
						AlertDialog.Builder logOutDialog = 
								new AlertDialog.Builder(
								BaseActivity.this);
						logOutDialog.setMessage(BaseActivity.this
								.getResources().getString(
										R.string.toast_check_save));

						logOutDialog.setPositiveButton("Yes",
								logoutDialogClickListener);

						logOutDialog.setNegativeButton("No",
								logoutDialogClickListener);

						logOutDialog.show();

					} else {

						// logout
						logOut();
					}
				} else {

					// if not logged in
					// direct user to RegisterActivity
					startActivity(new Intent(BaseActivity.this,
							RegisterActivity.class));
				}
				break;

			// add patient item
			case (3):
				if (BaseActivity.application.isLoggedIn()
						&& BaseActivity.application.getActiveAccount()
								.getUsergroupId() == 1) {
					i = new Intent(BaseActivity.this,
							GetPatientIDActivity.class);
					i.putExtra("add", true);
					startActivity(i);
				} else {
					Toast.makeText(BaseActivity.this,
							"You are not authorized to add a patient.",
							Toast.LENGTH_LONG).show();
				}

				break;

			// find patient item
			case (4):
				i = new Intent(BaseActivity.this, GetPatientIDActivity.class);
				i.putExtra("add", false);
				startActivity(i);
				break;

			// sort by urgency level item
			case (5):

				// give access to only nurse accounts
				if (BaseActivity.application.isLoggedIn()
						&& BaseActivity.application.getActiveAccount()
								.getUsergroupId() == 1) {
					application.getAppData().setConfig("isUrgencySortOrder",
							true);

					rightDrawerView = (DrawerView) findViewById(
							R.id.right_drawer);

					// gets the right drawers items (patients) from emergency
					// room
					drawerViewItems = application
							.getAppData()
							.getActive()
							.toDrawerItems(BaseActivity.this, true,
									application.getAppData().getPatients());

					// gives the right drawer click functionality
					rightDrawerView.setAdapter(new DrawerItemAdapter(
							BaseActivity.this, R.layout.drawer_item,
							drawerViewItems));
					// closes left drawer and opens right drawer
					closeLeftDrawer(view.getContext());
					openRightDrawer(view.getContext());
				} else {
					Toast.makeText(
							BaseActivity.this,
							"You are not authorized to add sort patients "
									+ "by urgency.", Toast.LENGTH_LONG).show();
				}
				break;

			// sort by arrival time item
			case (6):

				// give access to only nurse accounts
				if (BaseActivity.application.isLoggedIn()
						&& BaseActivity.application.getActiveAccount()
								.getUsergroupId() == 1) {
					application.getAppData().setConfig("isUrgencySortOrder",
							false);

					rightDrawerView = (DrawerView) findViewById(
							R.id.right_drawer);

					// gets the right drawers items (patients) from emergency
					// room
					drawerViewItems = application
							.getAppData()
							.getActive()
							.toDrawerItems(BaseActivity.this, false,
									application.getAppData().getPatients());

					// gives the right drawer click functionality
					rightDrawerView.setAdapter(new DrawerItemAdapter(
							BaseActivity.this, R.layout.drawer_item,
							drawerViewItems));
					// closes left drawer and opens right drawer
					closeLeftDrawer(view.getContext());
					openRightDrawer(view.getContext());

				} else {
					Toast.makeText(
							BaseActivity.this,
							"You are not authorized to sort patients by "
									+ "arrival time.", Toast.LENGTH_LONG)
							.show();
				}
				break;
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is
		// present.
		getMenuInflater().inflate(R.menu.base, menu);
		return true;
	}

	DialogInterface.OnClickListener logoutDialogClickListener = 
			new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:

				// logout
				logOut();

				// toast message to tell user that they have logged out
				Toast.makeText(
						BaseActivity.this,
						BaseActivity.this.getResources().getString(
								R.string.toast_logout_successfully),
						Toast.LENGTH_LONG).show();

				break;

			case DialogInterface.BUTTON_NEGATIVE:

				// closes messagebox
				dialog.dismiss();
				break;
			}
		}
	};

	/**
	 * Logs the current active Account out of the Application.
	 */
	private void logOut() {

		// clears the application data
		BaseActivity.application.setActiveAccount(null);

		// to move to MainActivity
		Intent intent = new Intent(BaseActivity.this, MainActivity.class);

		// clear top activities
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		startActivity(intent);
	}

	/**
	 * Saves the current Application Data to file (and remote if network is
	 * available)
	 */
	private void save() {

		// a method of saving the current data back to file.
		BaseActivity.application.saveData();

		// user saved
		isSaved = true;

		// closes left drawer
		closeLeftDrawer(this);

		// Display message after successfully saving data
		Toast.makeText(
				BaseActivity.this,
				BaseActivity.this.getResources().getString(
						R.string.toast_saved_successfully), Toast.LENGTH_LONG)
				.show();

	}

	/**
	 * Unlocks the right drawer (for those authorized to view it).
	 */
	private void unlockRightDrawer() {
		((DrawerLayout) ((BaseActivity) this).findViewById(R.id.app_frame))
				.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED,
						Gravity.RIGHT);
	}

	/**
	 * Locks the right drawer (for those not authorized to view it).
	 */
	private void lockRightDrawer() {
		((DrawerLayout) ((BaseActivity) this).findViewById(R.id.app_frame))
				.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,
						Gravity.RIGHT);

	}

	/**
	 * Opens the left drawer.
	 * 
	 * @param c
	 *            the activity context
	 */
	private void openLeftDrawer(Context c) {
		// opens left drawer
		((DrawerLayout) ((BaseActivity) c).findViewById(R.id.app_frame))
				.openDrawer(Gravity.LEFT);
	}

	/**
	 * Opens the right drawer.
	 * 
	 * @param c
	 *            the activity context
	 */
	private void openRightDrawer(Context c) {
		// opens right drawer
		((DrawerLayout) ((BaseActivity) c).findViewById(R.id.app_frame))
				.openDrawer(Gravity.RIGHT);
	}

	/**
	 * Closes the left drawer.
	 * 
	 * @param c
	 *            the activity context.
	 */
	private void closeLeftDrawer(Context c) {
		// closes left drawer
		((DrawerLayout) ((BaseActivity) c).findViewById(R.id.app_frame))
				.closeDrawer(Gravity.LEFT);
	}

	/**
	 * Sourced from:
	 * http://indyvision.net/2010/02/android-request-user-decision-
	 * when-back-button-pressed/
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(this, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
		}
		return true;
	}

	/**
	 * Gets Account for the User with Username username
	 * 
	 * @return account of the user with Username username
	 */
	@Override
	public Account getAccount(String username) {
		return BaseActivity.application.getAccount(username);
	}

	/**
	 * Return the account for the User with Username username and Password
	 * password
	 * 
	 * @return Account of the User with Username username and Password password
	 */
	@Override
	public Account getAccount(String username, String password) {
		return BaseActivity.application.getAccount(username, password);
	}
	
	/**
	 * Returns true iff account does not already exist else returns false
	 * 
	 * @return true iff account does not already exist else returns false
	 */
	@Override
	public boolean addAccount(Account account) {
		return BaseActivity.application.addAccount(account);
	}
	
	/**
	 * Sets the account as the Active Account in the application of this
	 * activity
	 */
	@Override
	public void setActiveAccount(Account account) {
		BaseActivity.application.setActiveAccount(account);
	}
	
	/**
	 * Gets the Active Account from the application of this Activity
	 */
	@Override
	public Account getActiveAccount() {
		return BaseActivity.application.getActiveAccount();
	}
	
	/**
	 * Returns true iff the user is logged in
	 * @return true iff the user is logged in
	 */
	@Override
	public boolean isLoggedIn() {
		return BaseActivity.application.isLoggedIn();
	}

	/**
	 * Checks if a network connection is available to use. Sourced from:
	 * http://stackoverflow.com/questions/4238921/android-detect-whether
	 * -there-is-an-internet-connection-available
	 * 
	 * @return true if network is available, false otherwise
	 */
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}