package ca.kenma.triage.view;

import ca.kenma.triage.R;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import ca.kenma.triage.model.*;

public class AddPrescriptionActivity extends BaseActivity {

	private String healthCardNumber;
	private Bundle extras;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// inflate xml so that we can manipulate the layout
		FrameLayout contentFrame = (FrameLayout)
				findViewById(R.id.content_frame);
		LayoutInflater inflater = (LayoutInflater)
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout prescriptionAddForm = (RelativeLayout) inflater.inflate(
				R.layout.prescriptionadd_form, null);
		RelativeLayout formLayout = (RelativeLayout) inflater.inflate(
				R.layout.form, null);
		((RelativeLayout) formLayout.findViewById(R.id.form_content))
				.addView(prescriptionAddForm);

		contentFrame.addView(formLayout);

		// change fonts and sets text of headers and descriptions
		TextView header = (TextView) findViewById(R.id.form_header_title);
		header.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		header.setText(this.getResources().getString(
				R.string.title_activity_prescription_add));

		TextView desc = (TextView) findViewById(R.id.form_header_text);
		desc.setTypeface(BaseActivity.getFont(this, "robotothinitalic.ttf"));
		desc.setText(this.getResources().getString(
				R.string.addprescription_form_desc));

		extras = getIntent().getExtras();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is
		// present.
		getMenuInflater().inflate(R.menu.add_prescription, menu);
		return true;
	}

	/**
	 * Adds a prescription to the patient.
	 * 
	 * @param view
	 *            The view of this activity.
	 */
	public void prescriptionAddSubmit(View view) {

		// get the health card number from the previous activity
		healthCardNumber = (String) extras.get("healthCardNumber");

		// set the text fields
		EditText medNameField = (EditText) findViewById(R.id.medication_name);
		String medName = medNameField.getText().toString();

		// set the text fields
		EditText medInfoField = (EditText) findViewById(R.id.medication_info);
		String medInfo = medInfoField.getText().toString();

		// if both med name and med info are filled in
		if (!medName.equals("") && !medInfo.equals("")) {

			// create a new prescription for the patient
			Prescription prescription = new Prescription(medName, medInfo);

			// get the reference to the correct patient
			Patient patientToEditER = BaseActivity.application
					.getAppData()
					.getActive()
					.getPatient(healthCardNumber,
							BaseActivity.application.getAppData()
							.getPatients());
			// adds the prescription to the patient
			patientToEditER.getLatestRecord().addPrescription(prescription);

			// create a new intent to go back to PatientViewActivity
			// and passing in the patient's health card number
			Intent intent = new Intent(this, PatientViewActivity.class);
			intent.putExtra("healthCardNumber", healthCardNumber);

			// display a toast message to tell the user prescription was added
			Toast.makeText(
					this,
					this.getResources()
							.getString(
									R.string
									.toast_successful_addprescription_prescription_added),
					Toast.LENGTH_LONG).show();

			// new prescription added, user did not save at this point
			BaseActivity.isSaved = false;

			// start the PatientViewActivity
			this.startActivity(intent);

		} else {

			// display a toast message to tell the user to enter all fields
			Toast.makeText(
					this,
					this.getResources().getString(
							R.string.toast_failed_addprescription_empty_fields),
					Toast.LENGTH_LONG).show();
		}
	}
}
